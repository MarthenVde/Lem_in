#include "../includes/lem.h"

static int index_of(t_env *env, char *dst) {
    t_room *room = find_room(env, dst);
    int index = -1;

    if (room)
        index = room->index;
    return index;
}

t_queue *get_paths(t_env *env) {
    t_room *room = NULL;
    t_node *neighbours = NULL;
    t_node *curr_path = NULL;
    t_queue *paths = NULL;
    t_queue *queue = NULL;
    t_node *new = NULL;
    char *last = NULL;
    int size = env->size;
    int visited[size];
    int i = 0;

    while (i < size) {
        visited[i] = 0;
        i++;
    }

    enqueue(&queue, create_node(env->start));
    while (queue) {
        curr_path = dequeue(&queue);
        last = last_in_path(curr_path);
        int index = index_of(env, last);

        if (visited[index] == 0) {
            room = find_room(env, last);
            neighbours = room->links;
            while (neighbours != NULL) {
                new = NULL;
                new = appended_path(&curr_path, neighbours->room);
                enqueue(&queue, new);

                if (ft_strequ(neighbours->room, env->end)  && room_count(new, env->end) == 1)
                    enqueue(&paths, new);

                neighbours = neighbours->next;
            }
            visited[index] = 1;
            curr_path = NULL;
            neighbours = NULL;
        }
    }
    return paths;
}

void    check_start_end(t_env *env) {
        t_room *tmp;

        tmp = env->head;
        while (tmp) {
            if (strcmp(tmp->name, env->start) == 0) {
                if (!tmp->links) {
                    ft_putstr("ERROR\n");
                    exit(1);
                }
            }
            else if (strcmp(tmp->name, env->end) == 0) {
                if (!tmp->links) {
                    ft_putstr("ERROR\n");
                    exit(1);
                }
            }
            tmp = tmp->next;
        }
}
